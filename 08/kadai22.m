% 課題2-2
NUM = 100;         % NUM * NUMの行列
A = zeros(NUM);   % Initialize
b = ones(1, NUM - 1);  % 要素のすべてが1でNUM 個の行
A = diag(ones(1, NUM) * 2) + diag(b, -1) + diag(b, 1);

x0 = ones(NUM, 1);

[eigenvec, eigenval] = powermethod(A, x0)