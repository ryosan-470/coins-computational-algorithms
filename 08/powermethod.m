% べき乗法
function [eigenvec, eigenval] = powermethod(A, x0)
STOPPED = 10^-5;  % 停止条件
x = x0;

while true
    t = A * x;
    eigenval = dot(x, t) / dot(x, x);
    r = norm(t - eigenval * x);
    if r < STOPPED
        break;
    end
    x = A * x;
    x = x / norm(x);
end
eigenvec = x;
