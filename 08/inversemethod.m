% 逆反復法
function [eigenvec, eigenval] = inversemethod(A, sigma)
I = eye(length(A));
G = inv(sigma * I - A);
x0 = ones(length(A), 1)
% 固有ベクトル, 固有値
[evec, eval] = powermethod(G, x0);

eigenval = sigma - 1 / eval;
eigenvec = G * evec;
