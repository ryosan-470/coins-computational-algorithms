function [rt, count, rnorm] = cg_extend(A, b, x0)
r = b - (A*x0);
p = r;
x = zeros;
stopped = 1.0E-4;
count = 1;
while true
    tmp = A*p;
    a = dot(p, r)/dot(p,tmp);
    x = x + a * p;
    r = r - a * tmp;
    rnorm(count) = norm(r);
    if rnorm(count) < stopped
        rt = x;
        break;
    end
    count = count + 1;
    beta   = - dot(r,tmp)/dot(p,tmp);
    p = r + beta * p;
end