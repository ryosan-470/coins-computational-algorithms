function [rt] = cg(A, b, x0)
r = b - (A*x0);
p = r;
x = zeros;
stopped = 1.0E-4;
while true
    tmp = A*p;
    a = dot(p, r)/dot(p,tmp);
    x = x + a * p;
    r = r - a * tmp;
    if norm(r) < stopped
        rt = x;
        break;
    end
    beta   = - dot(r,tmp)/dot(p,tmp);
    p = r + beta * p;
end