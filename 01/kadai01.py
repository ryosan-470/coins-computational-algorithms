#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

print(np.ones((4,3)))
print(np.eye(3))
print(np.zeros((3,4)))
# v = [1; 2]
v = np.array([[1], [2]])
print("v = ", end="") 
print(v)
# A = [1 2; 3 4]
A = np.array([[1, 2],
              [3, 4]])
print("A = ", end="")
print(A)
