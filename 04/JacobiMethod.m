function [u, cnt, Rs] = JacobiMethod (n, u)
stopped = 10^(-4);  % ????
cnt = 0;            % ????
R = 0;
max_loop = 1000;    % ??

tmp = repmat(u, 1);
for loop = 1:max_loop
    for i = 2:n+1
        for j = 2:n+1
            r = 1 / 4 * (u(i+1, j) + u(i-1,j) + u(i, j+1) + u(i,j-1)) - u(i,j);
            tmp(i, j) = u(i,j) + r;
            if R < abs(r)
                R = abs(r);
            end
        end
    end
    cnt = cnt + 1;
    Rs(cnt) = R;
    u = tmp;
    if R <= stopped
       break
    end
    R = 0;
end