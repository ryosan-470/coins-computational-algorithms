n = 50;
u = zeros(n+2, n+2);
u(:,n+2) = 100;
w = linspace(0.01, 1.99, 199);
cnt = zeros(199, 1);

for k=1:199
   [u, cnt(k), Rs] = SORMethod(n, u, w(k));
   u = zeros(n+2, n+2);   % ???
   u(:,n+2) = 100;
end

fi = semilogy(w, cnt);
saveas(fi, '~/public_html/secure_htdocs/matlab/coma/04/kadai22', 'pdf')