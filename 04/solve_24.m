n = 50;
u = zeros(n+2, n+2);
u(:,n+2) = 100;

for k = 2:n+1
u(k,1) = k * (k - 1);
u(k,n+2) = -k * (k - 1);
u(1,k) = -k * (k - 1);
u(n+2,k) = k * (k - 1);
end

w = 1.5;

[u, count, Rs] = SORMethod(n, u, w)
fi = surf(u)
saveas(fi, '~/public_html/secure_htdocs/matlab/coma/04/kadai24', 'pdf')