function [u, cnt, Rs] = SORMethod (n, u, w)
stopped = 10^(-4);  % ????
cnt = 0;            % ????
R = 0;
max_loop = 1000;    % ??????
for loop = 1:max_loop
    for i = 2:n+1
        for j = 2:n+1
            r = 1 / 4 * (u(i+1, j) + u(i-1,j) + u(i, j+1) + u(i,j-1)) - u(i,j);
            u(i, j) = u(i,j) + (w * r);
            if R < abs(r)
                R = abs(r);
            end
        end
    end
    cnt = cnt + 1;
    Rs(cnt) = R;
    if R <= stopped
       break
    end
    R = 0;
end