n = 50;
u = zeros(n+2, n+2);
u(:,n+2) = 100;
w = 1.5;

[u, cnt, Rs] = SORMethod(n, u, w);

fi = semilogy(Rs);
saveas(fi, '~/public_html/secure_htdocs/matlab/coma/04/kadai21', 'pdf')