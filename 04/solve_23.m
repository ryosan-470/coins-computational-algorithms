n = 50;
u = zeros(n+2, n+2);
u(:,n+2) = 100;
w = 1.5  % ??????????

[uj, cntj, Rsj] = JacobiMethod (n, u);
[ug, cntg, Rsg] = SORMethod(n, u, 1);  % SOR
[ux, cntx, Rsx] = SORMethod(n, u, 1.89);


fi = semilogy(Rsj, 'r');
hold on;
fi = semilogy(Rsg, 'y');
hold on;
fi = semilogy(Rsx, 'b');
saveas(fi, '~/public_html/secure_htdocs/matlab/coma/04/kadai23', 'pdf')