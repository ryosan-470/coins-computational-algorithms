% 課題1-2
% sigmaと反復回数
clear all;
A = [1 1 1; 1 2 2; 1 2 3]
B = [2 1 0; 1 2 1; 0 1 2]

sigma = linspace(0, 3, 10000);
v = rand(length(A), 1);
for i=1:length(sigma)
    [eigenvec, eigenval, iter, r] = solver_gep(A, B, sigma(i), v);
    iters(i) = iter;
end

h = semilogy(sigma, iters)
saveas(h, '~/public_html/secure_htdocs/matlab/coma/09/kadai121.pdf','pdf')
