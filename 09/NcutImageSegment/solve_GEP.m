% 画像領域分割
% W, D  : 正方行列
% sigmaに近い固有値に対応する固有ベクトルをXに返却
% 反復回数をiter回とする
% (D - W) x = lambda * D * x
function X = solve_GEP(W, D, sigma, iter)
    A = D - W;
    v = A * rand(length(A), 1);
    R = chol(D);   % コレスキー分解
    D = R' * R;
    T = inv(A - sigma * D);
    for i=1:iter
        w = R * T * R' * v;
        v = w / norm(w);
        i
    end
    X = inv(R) * v;
    X = X / norm(X)
end