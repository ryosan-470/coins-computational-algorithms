% 課題1-1
A = [1 1 1; 1 2 2; 1 2 3]
B = [2 1 0; 1 2 1; 0 1 2]
sigma = 1
v = rand(length(A), 1);
[eigenvec, eigenval, iter, r] = solver_gep(A, B, sigma, v)