% 課題1-2
% x軸反復回数 y軸は残差
A = [1 1 1; 1 2 2; 1 2 3];
B = [2 1 0; 1 2 1; 0 1 2];

v = rand(length(A), 1);
% sigma = 1.1
[evec, eval, iter, r] = solver_gep(A, B, 1.1, v);
s = linspace(0, iter, iter+1);
h = loglog(s, r, 'k')
hold on;

% sigma = 1.0
[evec, eval, iter, r] = solver_gep(A, B, 1.0, v);
s = linspace(0, iter, iter+1);
h = loglog(s, r, 'c')
hold on;

% sigma = 0.8
[evec, eval, iter, r] = solver_gep(A, B, 0.8, v);
s = linspace(0, iter, iter+1);
h = loglog(s, r, 'm')
hold on;

% sigma = 0.6
[evec, eval, iter, r] = solver_gep(A, B, 0.6, v);
s = linspace(0, iter, iter+1);
h = loglog(s, r, 'y')
hold on;

% sigma = 0.4
[evec, eval, iter, r] = solver_gep(A, B, 0.4, v);
s = linspace(0, iter, iter+1);
h = loglog(s, r, 'r')
hold on;

% sigma = 0.2
[evec, eval, iter, r] = solver_gep(A, B, 0.2, v);
s = linspace(0, iter, iter+1);
h = loglog(s, r, 'g')
hold on;

% sigma = 0
[evec, eval, iter, r] = solver_gep(A, B, 0.0, v);
s = linspace(0, iter, iter+1);
h = loglog(s, r, 'b')
hold on;

legend('sigma = 1.1', 'sigma = 1.0' , 'sigma = 0.8', 'sigma = 0.6', 'sigma = 0.4', 'sigma = 0.2', 'sigma = 0.0');

saveas(h, '~/public_html/secure_htdocs/matlab/coma/09/kadai122_loglog.pdf','pdf')