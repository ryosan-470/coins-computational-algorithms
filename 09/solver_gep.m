% Solver generalized eigenvalue problem
function [eigenvec, eigenval, iter, r] = solver_gep(A, B, sigma, v)
STOPPED = 10^(-5);
iter = 0;
R = chol(B);
B = R' * R;

T = A - sigma * B;
while true
    w = R * inv(T) * R' * v;
    eigenval = (1 / (transpose(v) * w)) + sigma;
    v = w / norm(w);

    x = inv(R) * v;
    r(iter + 1) = norm(A*x - eigenval * B * x);
    if r(iter + 1) <= STOPPED
        break
    end
    iter = iter + 1;
end
eigenvec = x / norm(x);
