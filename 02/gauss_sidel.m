n = 5;
u = zeros(n+2, n+2);
u(:,n+2) = 100;

for i = 2:n+1
    for j = 2:n+1
        r = 1 / 4 * (u(i+1, j) + u(i-1, j) + u(i, j+1) + u(i, j-1)) - u(i,j);
        u(i, j) = u(i, j) + r;
    end
end