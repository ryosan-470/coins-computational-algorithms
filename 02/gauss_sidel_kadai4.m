n = 50;
u = zeros(n+2, n+2);
u(:,n+2) = 100;
cnt = 1;
R = 0;
Rs(1) = 0;
while true
    for i = 2:n+1
        for j = 2:n+1
            r = 1 / 4 * (u(i+1, j) + u(i-1, j) + u(i, j+1) + u(i, j-1)) - u(i,j);
            u(i, j) = u(i, j) + r;
            if R < abs(r)
                R = abs(r);
                Rs(cnt) = R;
            end
        end
    end
    if R <= 10^(-4)
       u(i,  j);
       break
    end
    R = 0;
    cnt = cnt + 1;
end

x = 1:cnt;
% pg = plot(x, Rs);
% saveas(pg, '~/public_html/secure_htdocs/matlab/coma/02/kadai4_plot.pdf', 'pdf');

% px = semilogx(x, Rs);
% saveas(px, ['~/public_html/secure_htdocs/matlab/coma/02/' 'kadai4_semilogx.pdf'], 'pdf');
 
% py = semilogy(x, Rs);
% saveas(py,'~/public_html/secure_htdocs/matlab/coma/02/kadai4_semilogy.pdf','pdf');  
pl = loglog(x, Rs);
saveas(pl, '~/public_html/secure_htdocs/matlab/coma/02/kadai4_loglog.pdf', 'pdf');

t = 0:n+1;
saveas(surf(t,t,u), '~/public_html/secure_htdocs/matlab/coma/02/kadai5.pdf', 'pdf');
