n = 50;
u = zeros(n+2, n+2);
for t=1:n+2
    u(t, 1) = sin(pi*(t-1)/(n+1));
    u(n+2, t) = sin(pi*(t-1)/(n+1));
end

cnt = 1;
R = 0;
Rs(1) = 0;
while true
    for i = 2:n+1
        for j = 2:n+1
            r = 1 / 4 * (u(i+1, j) + u(i-1, j) + u(i, j+1) + u(i, j-1)) - u(i,j);
            u(i, j) = u(i, j) + r;
            if R < abs(r)
                R = abs(r);
                Rs(cnt) = R;
            end
        end
    end
    if R <= 10^(-4)
       u(i,  j);
       break
    end
    R = 0;
    cnt = cnt + 1;
end
x = 0:n+1;

saveas(surf(x,x,u), '~/public_html/secure_htdocs/matlab/coma/02/kadai6.pdf', 'pdf');